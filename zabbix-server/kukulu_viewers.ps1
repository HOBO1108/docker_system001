#!/usr/bin/pwsh

### 現在のKukuluLIVEの総視聴者数を確認するツール

# 総視聴者数取得関数
function kukulu_viewers() {
    $KUKULU =  Invoke-RestMethod -Uri https://live.erinn.biz/api/?category=live
    (($KUKULU).live.viewers | Measure-Object -Sum).Sum
}

    kukulu_viewers
